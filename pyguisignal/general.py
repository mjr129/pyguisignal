import os
import re
from os import path

from pyguisignal.dialogue_compiler import process_dialogue
from pyguisignal.configuration import Config
from pyguisignal.resource_compiler import process_resource


def run_system( command: str ) -> None:
    result = os.system( command )
    
    if result:
        raise ValueError( "Unhappy result '{0}' from '{1}'".format( result, command ) )


def process_unknown( config: Config, root: str, file_name: str ):
    process_function( config, root, file_name, config.dialogue_designer, config.dialogue_initialisation, config.dialogue_logic, process_dialogue )
    process_function( config, root, file_name, config.resource_designer, config.resource_initialisation, config.resource_logic, process_resource )


def process_function( config: Config, root: str, file_name: str, wildcard_designer: str, wildcard_init: str, wildcard_logic: str, function ):
    file_name = path.abspath( file_name )
    
    if re.fullmatch( wildcard_designer, file_name ):
        filename_init = re.sub( wildcard_designer, wildcard_init, file_name )
        filename_logic = re.sub( wildcard_designer, wildcard_logic, file_name )
        function( config, root, file_name, filename_init, filename_logic )
