import re


class Config:
    def __init__( self ) -> None:
        ###############################################################################################################
        #                                                                                                             #
        # PROJECT STRUCTURE                                                                                           #
        #                                                                                                             #
        ###############################################################################################################
        
        # Input dialogue file pattern matching
        self.dialogue_designer = "(.+/)(.+/)(.+)(_designer)\\.ui"
        
        # Generated dialogue file (from `dialogue_designer`, use "\\k" for capture group k (in range 1..n)) 
        self.dialogue_initialisation = "\\1\\2\\3\\4.py"
        
        # Dialogue logic file, where * matches the * in `dialogue_designer`
        # This is used to specify where the designer logic file is located.
        # This file should already exist, but a new one will be created if it does not.
        self.dialogue_logic = "\\1\\3.py"
        
        # Input resource file pattern matching, where `*` is the wildcard
        # This is used to locate the input resource files 
        self.resource_designer = "(.+)\\.qrc"
        
        # Generated resource file, where * matches the * in `resource_designer`
        # This is used to specify the output names of the resource files
        self.resource_initialisation = "\\1_rc.py"
        
        # We'll create a file containing constants specifying the names of your resources.
        # The * matches the * in the `resource_designer` attribute.
        # This is used to specify the output file name of the resource constants file
        self.resource_logic = "\\1.py"
        
        
        
        ###############################################################################################################
        #                                                                                                             #
        # SIGNAL PROCESSING                                                                                           #
        #                                                                                                             #
        ###############################################################################################################
        
        # The signals to create
        #
        # This is a Dictionary[key, value] where:
        #        key   : str = Name of widget
        #        value : Tuple[n] =
        #            value[0...n] : Tuple[2] =
        #                 value[0] : str = signal
        #                 value[1] : str = arguments
        self.handlers = \
            {
                "QPushButton"       : [["clicked", ""]],
                "QAction"           : [["triggered", ""]],
                "QCommandLinkButton": [["clicked", ""]],
                "QToolButton"       : [["clicked", ""]],
                "QDialogButtonBox"  : [["accepted", ""], ["rejected", ""]],
            }
        
        # How we format the signal handlers, where the following substitutions are made:
        #       {0}: Name
        #       {1}: Type
        #       {2}: Signal
        #       {3}: Params
        #       {4}: Function name (i.e. `self.function_format`)
        #       {5}: Formatted parameters, including "self"
        self.command_format = \
            """@exqtSlot({3})
            def {4}({5}) -> None:
                \"\"\"
                Signal handler:
                \"\"\"
                pass
            """
        
        # How we format the function names (this uses the same formatting parameters as the `command_format` field)
        self.function_format = "on_{0}_{2}"
        
        # The following lines will be removed from the dialogue initialisation code
        #
        # This is a Dictionary[key, value] where:
        #   key   : str = find
        #   value : str = replace
        self.garbage_regex = [
            # Swap our setupUi method for a constructor, so the IDE can see the field types properly
            [re.escape( r"def setupUi(self, Dialog):" ), r"def __init__(self, Dialog):"],  
            # Get rid of these constants that don't exist in PyQt5
            [re.escape( r"QtWidgets.QDockWidget.DockWidgetFeatureMask" ), r"QtWidgets.QDockWidget.DockWidgetClosable | QtWidgets.QDockWidget.DockWidgetFloatable | QtWidgets.QDockWidget.DockWidgetMovable | QtWidgets.QDockWidget.DockWidgetVerticalTitleBar"],
            # Don't assume our resources_rc lives in the same folder as the dialogue 
            [re.escape( r"import resources_rc" ), ""]
        ]
        
        ###############################################################################################################
        #                                                                                                             #
        # ADVANCED SETTINGS                                                                                           #
        #                                                                                                             #
        ###############################################################################################################
        
        # The following settings represent internal workings and generally shouldn't be changed
        
        # System command used to compile resources
        self.resource_command = 'pyrcc5 "{0}" -o "{1}"'
        
        # System command to compile dialogues
        self.ui_command = 'pyuic5 "{0}" -o "{1}"'
        
        # Regular expression used to find the widgets
        self.widget_regex = r"self\.(.*?) = QtWidgets\.(.*?)\("
        
        # Verbose status. This can also be changed via the command line's `--verbose` option.
        self.verbose = False
        
        # When set, any file in the path, which has the same name as the resource logic/constants file, will be replaced with
        # a copy of the resource logic/constants file. This is useful if multiple projects need access to the resource
        # constants.
        self.multi_resource = True


