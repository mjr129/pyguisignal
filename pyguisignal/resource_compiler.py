from mhelper import file_helper, ansi_format_helper, ansi
from pyguisignal.configuration import Config
from pyguisignal import general


def process_resource( config: Config, root: str, designer_filename: str, init_filename: str, logic_filename: str ):
    """
    Processes the resource "qrc" file
    
    :param config:                  Configuration
    :param root:                    Project root.
    :param designer_filename:       .QRC file (in) 
    :param init_filename:           Compile _RC.PY file (out) 
    :param logic_filename:          Created .PY logic file (out) 
    :return: 
    """
    
    #
    # Create logic file
    #
    content = file_helper.read_all_text( designer_filename )
    
    result = []
    result.append( '"""Resources names. File automatically generated by PyGuiSignal. Modifications may be overwritten."""' )
    result.append( "from mhelper.qt_resource_objects import ResourceIcon" )
    
    spec_prefix = None
    
    for line in content.split( "\n" ):
        line = line.strip()
        
        if line.startswith( '<qresource prefix="' ) and line.endswith( '">' ):
            spec_prefix = line[19:-2]
            
            break
    
    if spec_prefix is None:
        print( "Resource file must specify prefix." )
        spec_prefix = input( "PREFIX=" )
    
    prefix = ":/" + spec_prefix + "/"
    
    num = 0
    
    r = []
    r.append( "<RCC>" )
    r.append( '    <qresource prefix="{}">'.format( spec_prefix ) )
    
    for file in file_helper.list_dir( file_helper.get_directory( designer_filename ) ):
        filename = file_helper.get_file_name( file )
        if filename.startswith( "." ):
            continue
        
        if not file_helper.get_extension( filename ).lower() in (".svg", ".png", ".jpg", ".css"):
            continue
        
        name = file_helper.get_filename_without_extension( file )
        name = name.replace( "-", "_" )
        r.append( "        <file>" + filename + "</file>" )
        result.append( '{} = ResourceIcon( "{}{}" )'.format( name, prefix, filename ) )
        num += 1
    
    r.append( "    </qresource>" )
    r.append( "</RCC>" )
    
    rs = "\n".join( r )
    has_new_content = rs != content
    
    if has_new_content:
        file_helper.write_all_text( designer_filename, rs )
    
    logic = "\n".join( result )
    file_helper.write_all_text( logic_filename, logic )
    
    #
    # Resources can be located in multiple places
    #
    xxf = []
    
    if config.multi_resource:
        fni = file_helper.get_file_name( logic_filename )
        for xfile in file_helper.list_dir( root, recurse = True ):
            if "site-packages" in xfile[len( root ):]:
                continue
            
            fn = file_helper.get_file_name( xfile )
            if fn == fni and xfile != logic_filename:
                file_helper.write_all_text( xfile, logic )
                xxf.append( xfile )
    
    #
    # QRC --> _RC.PY
    #
    command = config.resource_command.format( designer_filename, init_filename )
    general.run_system( command )
    
    resource = ansi.FORE_GREEN + "RESOURCE" + ansi.RESET
    print( "PROCESS {}: {} - {} resources{}".format( resource, ansi_format_helper.highlight_filename( designer_filename ), num, " - NEW CONTENT" if has_new_content else " - no new content" ) )
    
    if config.verbose:
        print( "        Designer: " + ansi_format_helper.highlight_filename( designer_filename ) )
        print( "            Init: " + ansi_format_helper.highlight_filename( init_filename ) )
        print( "           Logic: " + ansi_format_helper.highlight_filename( logic_filename ) )
        for x in xxf:
            print( "            Copy: {}".format( ansi_format_helper.highlight_filename( x ) ) )
