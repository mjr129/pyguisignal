import sys
from os import path
from mhelper import file_helper, io_helper, ansi, ansi_format_helper

from pyguisignal import general
from pyguisignal.configuration import Config


# NOTE: Main processing is initiated in `general.process_unknown`

def main() -> None:
    commands = sys.argv[1:]
    
    if not commands:
        commands = (".",)
    
    #
    # Print help if we don't have everything
    #
    if any( x in commands for x in ("help", "-help", "--help", "/help", "?", "-?", "--?", "/?", "h", "-h", "--h", "/h") ):
        print( "pyguisignal <options> <path>" )
        print( "    <path> one or more paths to \".ui\" files or folders." )
        print( "    If folders are selected they will be recursively searched for '.ui' and '.qrc' files." )
        print( "    <options>:" )
        print( "        --verbose" )
        print( "        --config  <file_name> : Loads a configuration file" )
        print( "        --save <file_name> : Creates a configuration file using the current configuration" )
        print( "    Config file preference:" )
        print( "        --config <file_name>" )
        print( "        <path>/pyguisignal.json" )
        print( "        ~/pyguisignal.json" )
        print( "        (default)" )
        raise SystemExit()
    
    next_command = 0
    
    cf = path.join( path.expanduser( "~" ), "pyguisignal.json" )
    if path.isfile( cf ):
        print( "CONFIG: {}".format( cf ) )
        config = io_helper.load_json( cf )
    else:
        config = Config()
    
    sup_dir = False
    sup_ver = False
    
    #
    # Parse the commands
    #
    for command in commands:
        if config.verbose:
            print( "command: [" + command + "]" )
        
        if next_command == 1:
            print( "CONFIG: {}".format( command ) )
            config = io_helper.load_json( command )
            sup_dir = True
        elif next_command == 2:
            print( "SAVE CONFIG: {}".format( command ) )
            io_helper.save_json( config, command )
        elif command == "--config":
            next_command = 1
        elif command == "--save":
            next_command = 2
        elif command == "--verbose":
            print( "SET VERBOSE" )
            sup_ver = True
            config.verbose = True
        elif path.isdir( command ):
            command = path.abspath( command ).strip()
            resource = ansi.FORE_YELLOW + "DIRECTRY" + ansi.RESET
            print( "PROCESS {}: ".format( resource ) + ansi_format_helper.highlight_filename( command ) )
            
            cf = path.join( command, "pyguisignal.json" )
            if not sup_dir and path.isfile( cf ):
                print( "CONFIG: {}".format( cf ) )
                config = io_helper.load_json( cf )
            
            if sup_ver:
                config.verbose = True
            
            for file in file_helper.list_dir( command, recurse = True ):
                general.process_unknown( config, command, file )
        else:
            print( "Argument «{}» rejected".format( command ) )


if __name__ == "__main__":
    main()
